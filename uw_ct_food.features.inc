<?php

/**
 * @file
 * uw_ct_food.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_food_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_food_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_food_node_info() {
  $items = array(
    'uw_ct_food' => array(
      'name' => t('Food'),
      'base' => 'node_content',
      'description' => t('Content type and information about food.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
