<?php

/**
 * @file
 * Template file for information page about a food item.
 *
 * Drupal path: locations-and-hours/daily-menu/%food.
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">
    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>

    <div class="content_node"<?php print $content_attributes; ?>>
      <?php
      hide($content);
      if (!empty($content['field_uw_ct_food_diet_type']['#items'])) {
        // If there are dietary type icons, display them.
        // This way of doing it feels very brute-forcey but at least it works.
        // Don't copy this as a good example.
        echo '<ul class="food-item-dietary-list">';
        foreach ($content['field_uw_ct_food_diet_type']['#items'] as $key => $item) {
          echo '<li>';
          $tid = $item['tid'];
          $icon_info = $content['field_uw_ct_food_diet_type'][$key]['taxonomy_term'][$tid]['field_uw_fs_dietary_type_icon']['#object']->field_uw_fs_dietary_type_icon[LANGUAGE_NONE][0];
          $title = $content['field_uw_ct_food_diet_type'][$key]['taxonomy_term'][$tid]['field_uw_fs_dietary_type_icon']['#object']->name;
          $icon['style_name'] = 'uw_fs_dm_diet_icon_style';
          $icon['path'] = $icon_info['uri'];
          $icon['width'] = $icon_info['width'];
          $icon['height'] = $icon_info['height'];
          $icon['alt'] = $icon_info['alt'];
          print theme_image_style($icon);
          print ' ' . check_plain($title);
          echo '</li>';
        }
        echo '</ul>';
      }
      print render($content['field_serving_size']);
      print render($content['field_diet_type']);
      print render($content['field_ingredients']);
      print render($content['field_allergens']);
      ?>
    </div>

    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif; ?>

    <?php if (!empty($content['links'])): ?>
      <div class="links"><?php print render($content['links']); ?></div>
    <?php endif; ?>

    <!-- Nutrition Facts Table -->
    <?php if (!empty($content['field_calories'][0]['#markup'])): ?>
    <section class="nutrition-facts">
      <table>
        <caption>
          <h2>Nutrition Facts</h2>
          <div>Per <?php print $content['field_serving_size'][0]['#markup']; ?></div>
        </caption>
        <thead>
          <tr>
            <th scope="col">Amount</th>
            <th scope="col">% Daily Value</th>
          </tr>
        </thead>
        <tbody>
        <?php if (!empty($content['field_calories'][0]['#markup'])): ?>
          <tr>
            <td colspan="2"><b>Calories</b> <?php print $content['field_calories'][0]['#markup']; ?></td>
          </tr>
        <?php endif;
        if (!empty($content['field_amount_fat'][0]['#markup']) && !empty($content['field_daily_value_fat'][0]['#markup'])): ?>
          </tbody>
          <tbody>
          <tr>
            <th scope="rowgroup"><b>Fat</b> <?php print $content['field_amount_fat'][0]['#markup']; ?></th>
            <td><?php print $content['field_daily_value_fat'][0]['#markup']; ?></td>
          </tr>
          <?php
          if (!empty($content['field_amount_saturated'][0]['#markup']) && !empty($content['field_daily_value_saturated'][0]['#markup'])):
            $rowspan = empty($content['field_amount_trans'][0]['#markup']) ? '' : ' rowspan="2"';
          ?>
            <tr class="sub">
              <th scope="row">Saturated <?php print $content['field_amount_saturated'][0]['#markup']; ?></th>
              <td<?php echo $rowspan; ?>><?php print $content['field_daily_value_saturated'][0]['#markup']; ?></td>
            </tr>
            <?php if (!empty($content['field_amount_trans'][0]['#markup'])): ?>
              <tr class="sub continued">
                <th scope="row">+ Trans <?php print $content['field_amount_trans'][0]['#markup']; ?></th>
              </tr>
            <?php endif;
          ?>
          </tbody>
          <tbody>
          <?php
          endif;
        endif;
        if (!empty($content['field_cholesterol'][0]['#markup'])): ?>
          <tr>
            <td colspan="2"><b>Cholesterol</b> <?php print $content['field_cholesterol'][0]['#markup']; ?></td>
          </tr>
        <?php endif;
        if (!empty($content['field_amount_sodium'][0]['#markup']) && !empty($content['field_daily_value_sodium'][0]['#markup'])): ?>
          <tr>
            <th scope="row"><b>Sodium</b> <?php print $content['field_amount_sodium'][0]['#markup']; ?></th>
            <td><?php print $content['field_daily_value_sodium'][0]['#markup']; ?></td>
          </tr>
        <?php endif;
        if (!empty($content['field_amount_carbohydrate'][0]['#markup']) && !empty($content['field_daily_value_carbohydrate'][0]['#markup'])): ?>
          </tbody>
          <tbody>
          <tr>
            <th scope="rowgroup"><b>Carbohydrate</b> <?php print $content['field_amount_carbohydrate'][0]['#markup']; ?></th>
            <td><?php print $content['field_daily_value_carbohydrate'][0]['#markup']; ?></td>
          </tr>
          <?php
          if (!empty($content['field_amount_fibre'][0]['#markup']) && !empty($content['field_daily_value_fibre'][0]['#markup'])): ?>
            <tr class="sub">
              <th scope="row">Fibre <?php print $content['field_amount_fibre'][0]['#markup']; ?></th>
              <td><?php print $content['field_daily_value_fibre'][0]['#markup']; ?></td>
            </tr>
          <?php endif;
          if (!empty($content['field_sugars'][0]['#markup'])): ?>
            <tr class="sub">
              <td colspan="2">Sugars <?php print $content['field_sugars'][0]['#markup']; ?></td>
            </tr>
          <?php endif; ?>
          </tbody>
          <tbody>
        <?php endif;
        if (!empty($content['field_protein'][0]['#markup'])): ?>
          <tr>
            <td colspan="2"><b>Protein</b> <?php print $content['field_protein'][0]['#markup']; ?></td>
          </tr>
        <?php endif;

        $nutrients = [
          'field_vitamin_a' => 'Vitamin A',
          'field_vitamin_c' => 'Vitamin C',
          'field_calcium' => 'Calcium',
          'field_iron' => 'Iron',
        ];
        foreach ($nutrients as $key => $label):
          if (!empty($content[$key][0]['#markup'])): ?>
            <tr class="minerals">
              <th scope="row"><?php print $label ?></th>
              <td><?php print $content[$key][0]['#markup']; ?></td>
            </tr>
          <?php endif;
        endforeach;
        ?>
        </tbody>
      </table>
      </section>
    <?php endif; ?>

    <div>
    <?php
      print render($content['field_body'][0]['#markup']);
    ?>
    </div>

  </div><!-- /node-inner -->
</div><!-- /node-->
