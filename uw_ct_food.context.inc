<?php

/**
 * @file
 * uw_ct_food.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_food_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_ct_food';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'uw_ct_food' => 'uw_ct_food',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'hours_of_operation-uw_ct_food_nutrition_information' => array(
          'module' => 'hours_of_operation',
          'delta' => 'uw_ct_food_nutrition_information',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['uw_ct_food'] = $context;

  return $export;
}
