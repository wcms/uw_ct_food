<?php

/**
 * @file
 * uw_ct_food.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_food_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_ct_food content'.
  $permissions['create uw_ct_food content'] = array(
    'name' => 'create uw_ct_food content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create uw_location content'.
  $permissions['create uw_location content'] = array(
    'name' => 'create uw_location content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_food content'.
  $permissions['delete any uw_ct_food content'] = array(
    'name' => 'delete any uw_ct_food content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_location content'.
  $permissions['delete any uw_location content'] = array(
    'name' => 'delete any uw_location content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_food content'.
  $permissions['delete own uw_ct_food content'] = array(
    'name' => 'delete own uw_ct_food content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_location content'.
  $permissions['delete own uw_location content'] = array(
    'name' => 'delete own uw_location content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in allergens'.
  $permissions['delete terms in allergens'] = array(
    'name' => 'delete terms in allergens',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_diet_type'.
  $permissions['delete terms in uw_diet_type'] = array(
    'name' => 'delete terms in uw_diet_type',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_ct_food content'.
  $permissions['edit any uw_ct_food content'] = array(
    'name' => 'edit any uw_ct_food content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_location content'.
  $permissions['edit any uw_location content'] = array(
    'name' => 'edit any uw_location content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_food content'.
  $permissions['edit own uw_ct_food content'] = array(
    'name' => 'edit own uw_ct_food content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_location content'.
  $permissions['edit own uw_location content'] = array(
    'name' => 'edit own uw_location content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in allergens'.
  $permissions['edit terms in allergens'] = array(
    'name' => 'edit terms in allergens',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_diet_type'.
  $permissions['edit terms in uw_diet_type'] = array(
    'name' => 'edit terms in uw_diet_type',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_ct_food revision log entry'.
  $permissions['enter uw_ct_food revision log entry'] = array(
    'name' => 'enter uw_ct_food revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'merge allergens terms'.
  $permissions['merge allergens terms'] = array(
    'name' => 'merge allergens terms',
    'roles' => array(),
    'module' => 'term_merge',
  );

  // Exported permission: 'merge uw_diet_type terms'.
  $permissions['merge uw_diet_type terms'] = array(
    'name' => 'merge uw_diet_type terms',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'term_merge',
  );

  // Exported permission: 'override uw_ct_food authored by option'.
  $permissions['override uw_ct_food authored by option'] = array(
    'name' => 'override uw_ct_food authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_food authored on option'.
  $permissions['override uw_ct_food authored on option'] = array(
    'name' => 'override uw_ct_food authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_food comment setting option'.
  $permissions['override uw_ct_food comment setting option'] = array(
    'name' => 'override uw_ct_food comment setting option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_food promote to front page option'.
  $permissions['override uw_ct_food promote to front page option'] = array(
    'name' => 'override uw_ct_food promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_food published option'.
  $permissions['override uw_ct_food published option'] = array(
    'name' => 'override uw_ct_food published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_food revision option'.
  $permissions['override uw_ct_food revision option'] = array(
    'name' => 'override uw_ct_food revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_food sticky option'.
  $permissions['override uw_ct_food sticky option'] = array(
    'name' => 'override uw_ct_food sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  return $permissions;
}
