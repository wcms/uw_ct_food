<?php

/**
 * @file
 * uw_ct_food.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_food_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_carbohydrates|node|uw_food|form';
  $field_group->group_name = 'group_carbohydrates';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_food';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Carbohydrates',
    'weight' => '9',
    'children' => array(
      0 => 'field_amount_carbohydrate',
      1 => 'field_daily_value_carbohydrate',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Carbohydrates',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-carbohydrates field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_carbohydrates|node|uw_food|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_fat|node|uw_food|form';
  $field_group->group_name = 'group_fat';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_food';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fat',
    'weight' => '6',
    'children' => array(
      0 => 'field_amount_fat',
      1 => 'field_daily_value_fat',
      2 => 'group_saturated',
      3 => 'group_trans_fat',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Fat',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-fat field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_fat|node|uw_food|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_fibre|node|uw_food|form';
  $field_group->group_name = 'group_fibre';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_food';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fibre',
    'weight' => '10',
    'children' => array(
      0 => 'field_amount_fibre',
      1 => 'field_daily_value_fibre',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Fibre',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-fibre field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_fibre|node|uw_food|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_saturated|node|uw_food|form';
  $field_group->group_name = 'group_saturated';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_food';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_fat';
  $field_group->data = array(
    'label' => 'Saturated',
    'weight' => '14',
    'children' => array(
      0 => 'field_amount_saturated',
      1 => 'field_daily_value_saturated',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Saturated',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-saturated field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_saturated|node|uw_food|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sodium|node|uw_food|form';
  $field_group->group_name = 'group_sodium';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_food';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Sodium',
    'weight' => '8',
    'children' => array(
      0 => 'field_amount_sodium',
      1 => 'field_daily_value_sodium',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Sodium',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-sodium field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_sodium|node|uw_food|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_trans_fat|node|uw_food|form';
  $field_group->group_name = 'group_trans_fat';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_food';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_fat';
  $field_group->data = array(
    'label' => '+ Trans',
    'weight' => '15',
    'children' => array(
      0 => 'field_amount_trans',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '+ Trans',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-trans-fat field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_trans_fat|node|uw_food|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('+ Trans');
  t('Carbohydrates');
  t('Fat');
  t('Fibre');
  t('Saturated');
  t('Sodium');

  return $field_groups;
}
